<?php  
class Client{
private $conn;
private $table_name = "Clientes";

public $Id;
public $ApyNom;
public $CategoriaIva;
public $Dni;
public $MovilPhone;
public $Phone;
public $MargenCompra;


// constructor
    public function __construct($db){
    $this->conn = $db;
    }

    public function getClient($data) {
        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE FirstName = " . $data;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->Id = $row['Id'];
            $this->ApyNom= $row['ApellidoNombre'];
            $this->CategoriaIva = $row['categoriaIva'];
            $this->Dni = $row['Dni'];
            $this->CelPhone = $row['Celular'];
            $this->Phone = $row['Telefono'];
            $this->MargenCompra = $row['MargenCompra'];

            return true;
        }
        public function getClientAll($data) {

            $query = "SELECT *
            FROM " . $this->table_name . "
            WHERE clienteid = ". $data;
    
            // bind the values from the form
            //$stmt->bindParam(':codibarra', $this->codibarra);
    
            // prepare the query
            $stmt = $this->conn->prepare($query);
    
            // sanitize
            $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
    
            // bind the values from the form
            $stmt->bindParam(':data', $this->data);
    
            // execute the query
            if($stmt->execute()){
    
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
                // assign values to object properties
                /**$this->Id = $row['Id'];
                 $this->ApyNom= $row['ApellidoNombre'];
                 $this->CategoriaIva = $row['categoriaIva'];
                 $this->Dni = $row['Dni'];
                 $this->CelPhone = $row['Celular'];
                 $this->Phone = $row['Telefono'];
                 $this->MargenCompra = $row['MargenCompra'];

     
                return $row;
            }    
        }
    }
    
}

?>
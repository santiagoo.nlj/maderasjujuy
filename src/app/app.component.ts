import { Component, HostListener } from "@angular/core";
import { Platform, MenuController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";
import { AuthenticationService } from "./authentication/authentication.service";
import { ScreenService } from "./app-screen.service";
import { MenuData } from "./app-menu.data";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "./components/popover/popover.component";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  menuMetaData: any;
  userLogged: boolean = false;
  popover: any;
  currentUser: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private menuCtrl: MenuController,
    private authenticationService: AuthenticationService,
    private screen: ScreenService,
    public popoverController: PopoverController,
    public storage: Storage
  ) {
    this.initializeApp();
    this.menuMetaData = MenuData;
    this.storage.get("DASH_USER").then(val => {
      this.currentUser = val;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.authenticationService.authentication.subscribe(auth => {
        if (!auth.state) {
          this.menuCtrl.enable(false);
          this.router.navigate(["login"]);
        } else {
          console.log('profile');
          this.router.navigate(["dashboard"]);
        }
      });
    });
  }

  logout() {
    this.authenticationService.logout();
  }

  parseUrl(url) {
    return `/${url.split("/")[1]}/${url.split("/")[2]}`;
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.screen.onResize();
  }

  async profile(ev: any) {
    /* const popoverController = document.querySelector('ion-popover-controller');
    await popoverController.componentOnReady(); */
    
    
    this.popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      animated: true,
      showBackdrop: true,
      translucent: true,
      componentProps: {
        title: "Titulo Prueba",
        items: [
          {
            label: "Perfil",
            icon: "contact",
            path: "/dashboard/users/profile",
            visible: true,
            enable: true
          } /* ,
          {
            label: "Cerrar Sesion",
            icon: "log-out",
            path: "/dashboard",
            visible: true,
            enable: true
          } */
        ],
        dismiss: () => {
          this.popover.dismiss();
        }
      }
    });
    return await this.popover.present();
  }
}

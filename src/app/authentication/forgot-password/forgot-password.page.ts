import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.page.html",
  styleUrls: ["./forgot-password.page.scss"]
})
export class ForgotPasswordPage implements OnInit {
  recoveryForm: FormGroup;
  recoveryFailed: boolean = false;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.recoveryForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }

  onRecoverySubmit() {
    if (this.recoveryForm.invalid) {
      return;
    }
    //vamos a la API
  }
}

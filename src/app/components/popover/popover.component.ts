import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';


@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss']
})
export class PopoverComponent implements OnInit {

  title: string;
  items: any;
  dismiss: any;

  constructor(private navParams: NavParams,) { 
    this.title = this.navParams.get('title');
    this.items = this.navParams.get('items');
    this.dismiss = this.navParams.get('dismiss');
  }

  ngOnInit() {
  }

  closePopover() {
    this.dismiss();
  }

}

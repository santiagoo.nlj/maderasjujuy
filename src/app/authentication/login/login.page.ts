import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  loginFailed: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public auth: AuthenticationService
  ) { }
  
  ngOnInit(){
    this.loginForm = this.formBuilder.group({
      email:['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onLoginSubmit(){
    if (this.loginForm.invalid) {
      return;
    }
    this.auth.login(this.loginForm.value.email, this.loginForm.value.password, (resp)=> {
      console.log(resp);
      this.loginFailed = !resp;
    });
  }
}

import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, AlertController, NavController, NavParams } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ImageCropperComponent } from 'ngx-image-cropper';

@Component({
  selector: 'page-crop',
  templateUrl: 'cropper.html',
})
export class CroppImagePage {
  @ViewChild(ImageCropperComponent, { static: true })
  
  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageCropper: ImageCropperComponent;
  inputDisabled: boolean = false;
  myImage = null;
  onSave = null;
  height = 0;
  width = 0;
  ratio = "4 / 3";

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public alertCtrl: AlertController

  ) {
    console.log("CroppImagePage Contructor");

    this.myImage = params.get('image');
    this.height = params.get('size').height; // Maybe need computation
    this.width = params.get('size').width;
    this.onSave = params.get('onSave');
    console.log(this.myImage);
    console.log(this.height);
    this.inputDisabled=true;

    this.ratio = (this.height === this.width ? "1" : "16 / 9");
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: any) {
    this.croppedImage = event.base64;
    //alert("Width: " + event.width + "    Height: " + event.height);
  }
  save() {
    //event.file;
    this.onSave(this.croppedImage);
  }

  imageLoaded() {
    // show cropper
    this.inputDisabled = true;
  }
  loadImageFailed() {
    alert("Imagen Incorrecta");
  }

  clear() {
    this.inputDisabled = false;
    this.imageChangedEvent = null;
  }
}

@Component({
  selector: 'app-users-profile',
  templateUrl: './users-profile.component.html',
  styleUrls: ['./users-profile.component.scss'],
  entryComponents: [CroppImagePage]
})
export class UsersProfileComponent implements OnInit {
  @Output() userUpdated = new EventEmitter<Boolean>();

  userForm: FormGroup;
  user: any;
  passwordForm: FormGroup;
  password = {
    oldPassword: "",
    newPassword: "",
    repPassword: ""
  }
  dataImg: any;
  files: any;
  imagen: null;

  croppModal: any;
  imageResponse: any;
  options: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    public modalCtrl: ModalController,
    /* private userService: VariousService, */) {
    
    //this.user = JSON.parse(localStorage.getItem('user'));
    this.user = {
      dni: 12345678,
      first_name: "Javier",
      email: "mail@mail.com",
      last_name: "Caniparoli"
    }
    //console.log(this.user);

    this.createForm();
  }

  ngOnInit() {
    this.userForm.reset({
      dni: this.user.dni,
      first_name: this.user.first_name,
      email: this.user.email,
      last_name: this.user.last_name
    });

    this.passwordForm.reset({
      oldPassword: "",
      newPassword: "",
      repPassword: ""
    });
  }

  createForm() {
    this.userForm = this.fb.group({
      id: 0,
      dni: 0,
      last_name: "",
      first_name: "",
      email: "",
      password: ""
    });

    this.passwordForm = this.fb.group({
      oldPassword: "",
      newPassword: "",
      repPassword: ""
    });
  }

  changePassword() {
    /* this.userService.changePassword(this.password)
      .subscribe((data) => {
        if (JSON.parse(data['_body']).status) {
          alert("Cambio OK");
          localStorage.removeItem('isLoggedin')
          localStorage.removeItem('jwt')
          localStorage.removeItem('user')
          this.router.navigate(['/login']);
        } else {
          alert("No se pudo modificar");
        }
      }); */
      console.log(this.passwordForm);
      
  }

  cancel() {
    this.router.navigate(['dashboard/users']);
  }
  
  onSubmit() {
    this.user = this.prepareSaveUser();
    console.log(this.user);
    
    /* this.userService.updateUser(this.user)
      .subscribe((data) => {
        if (JSON.parse(data['_body']).status) {
          this.userService.updateUserGroup(this.user['user-group'][0])
            .subscribe((data) => {
              if (JSON.parse(data['_body']).status) {
                this.ngOnInit();
                //this.userUpdated.emit(true);
              } else {
                alert("No se pudo modificar");
              }
            });
        } else {
          alert("No se pudo modificar");
        }
      }); */
  }

  prepareSaveUser(): any {
    const formModel = this.userForm.value;

    const saveUser: any = {
      id: this.user.id,
      dni: formModel.dni as number,
      first_name: formModel.first_name as string,
      last_name: formModel.last_name as string,
      email: formModel.email as string,
      password: formModel.password as string,
    };
    return saveUser;
  }

  changeListener($event): void {
    this.files = $event.target.files;
    console.log($event);
    console.log($event.srcElement.offsetHeight, "|", $event.srcElement.offsetWidth);
    
    this.croppImage(this.files);
  }

  async croppImage(fileList: FileList) {
    if (fileList.length > 0) {
      let dataUrl;
      var reader = new FileReader();
      let imgSize = { width: 600, height: 600};
      reader.onloadend = async (e: Event) => {
        dataUrl = reader.result;
        console.log(dataUrl);
        this.croppModal = await this.modalCtrl.create({
          component: CroppImagePage,
          componentProps: { 
            image: dataUrl, 
            size: imgSize,
            onSave: this.changeImg
          }
        });
        return this.croppModal.present();
      }
      reader.readAsDataURL(fileList[0]);
    }   
  }

  changeImg = (userPic: any) => {
    this.imagen = userPic;
    this.croppModal.dismiss();
  };

  
}


import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
      path:"",
      redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: "./home/home.module#HomePageModule"
  },/*,
  {
    path: "users",
    loadChildren: "./users/users.module#UsersPageModule"
  },
  {
    path: "groups",
    loadChildren: "./groups/groups.module#GroupsModule"
  },
  {
    path: "clients",
    loadChildren: "./clients/clients.module#ClientsPageModule"
  },
  {
    path: "cars",
    loadChildren: "./cars/cars.module#CarsModule"
  },
  {
    path: "about",
    loadChildren: "./about/about.module#AboutPageModule"
  },
  { 
    path: 'drivers',
    loadChildren: './drivers/drivers.module#DriversPageModule'
  },
  { 
    path: 'travels',
    loadChildren: './travels/travels.module#TravelsPageModule'
  },
  {
    path: 'map',
    loadChildren: './map/map.module#MapPageModule'
  }*/
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DashboardRoutingModule {}
